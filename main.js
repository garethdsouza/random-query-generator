/** 
 * GARETH DSOUZA
 * CSE 6331 001 Spring 2018
 * Assignment 3
*/

/**
 * The main logic for rendering and running queries is present in this file.
 */

const express = require('express');
const router = express.Router();
const { Pool } = require('pg');
const connectionString = 'postgres://gareth:passwordg@picture-uploader-db.ctniqwa4r4ya.us-east-2.rds.amazonaws.com/pictureUploaderDB'
const baseQuery = "select * from earthquake_data "
const pool = new Pool({
    connectionString: connectionString,
})

/* GET home page. */
router.get('/', async function (req, res) {
    const numberOfTimesToRunQuery = req.query.noOfQueries;
    const minMag = req.query.minMag;
    const maxMag = req.query.maxMag;
    let result = {};
    if (numberOfTimesToRunQuery) {
        result = await generateNRandomQueries(numberOfTimesToRunQuery,minMag,maxMag);
    }
    res.render('index', { title: 'Random Query Generator', queryResults: result })
});

router.get('/executeQuery', async function (req, res, next) {
    const numberOfTimesToRunQuery = req.query.noOfQueries;
    try {
        const avgTimeTaken = await generateNRandomQueries(numberOfTimesToRunQuery)
        res.status(200).send({ avgTimeTaken: avgTimeTaken })
    } catch (err) {
        console.log("=======" + err)
        res.sendStatus(500)
    }

});


/**
 * Generates 'n' random queries and returns the total time, avg time and no of queries executed
 * @param {*} noOfQueries to be executed
 * @param {*} minMag minimum magnitued value 
 * @param {*} maxMag maximum magnitued value 
 */
const generateNRandomQueries = async function (noOfQueries,minMag,maxMag) {

    const queryTimes = []
    try {
        const client = await pool.connect()
        console.log("connected")
        for (i = 0; i < noOfQueries; i++) {
            let extra = '';
            //Checks to see if the minimum and max values are there and where clause is added.
            if(minMag != undefined && maxMag != undefined ){
                extra += "where mag between "+minMag+" and "+maxMag;
            }
            extra += " limit " + Math.floor(Math.random() * 50)
            extra += " offset " + Math.floor(Math.random() * 10)
            const startTime = new Date().getTime();
            console.log("QUERY: "+baseQuery + extra)
            await client.query(baseQuery + extra)
            queryTimes.push(new Date().getTime() - startTime)

        }
        await client.release()
        console.log("Client released")
    }
    catch (err) {
        console.log("ERROR" + err)
        client.release()
    }
    const totalTimeTaken = queryTimes.reduce((total, time) => total + time, 0)
    console.log("Avg time taken per query " + totalTimeTaken / queryTimes.length)
    return { totalTime: totalTimeTaken, avgTime: (totalTimeTaken / queryTimes.length).toFixed(2), noOfQueriesRan: noOfQueries };
}

module.exports = router;
